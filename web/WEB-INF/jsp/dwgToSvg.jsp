<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DWG图元转换为SVG图元</title>
  <link rel="stylesheet" href="../../css/bootstrap.min.css">
  <script src="../../js/jquery-3.5.1.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>

  <style>
    body {
      width: 100%;
      height: 100%;
      min-width: 1400px;

    }

    #title {
      color: rgb(127, 178, 255);
    }

    .main-content {
      margin-top: 20px;
      border: 5px solid rgb(178, 209, 255);
      padding-top: 20px;
    }

    #c13 {
      display: block;
    }

    .mydefine {
      width: 800px;
    }

    .main-content {
      position: relative;
    }

    .text-angel{
      font-size: small;
      color: grey;
    }

    .fileList {
      /* position: absolute; */
      border: 5px solid rgb(139, 255, 236);
      float: right;
      width: 500px;
      margin-top: -700px;
      margin-right: 30px;
      /* right: 30px;
      top: 20px; */
    }

    table {
      text-align: center;
    }

    .tableTitle {
      font-size: 20px;
      color: rgb(80, 98, 255);
    }

  </style>
</head>
<body>
  <div class="container">
    <div class="row">
      <h1 id="title">DWG图元转换为SVG图元</h1>
    </div>
    <div class="row main-content">
      <div class="mydefine">
        <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/upload" enctype="multipart/form-data">

          <!-- 页宽 -->
          <div class="form-group">
            <label for="c1" class="col-sm-3 control-label">页宽</label>
            <div class="col-sm-4">
              <input type="text" id="c1" class="form-control" name="pageWidth" placeholder="请输入页宽" required>
            </div>
          </div>

          <!-- 页高 -->
          <div class="form-group">
            <label for="c2" class="col-sm-3 control-label">页高</label>
            <div class="col-sm-4">
              <input type="text" id="c2" class="form-control" name="pageHeight" placeholder="请输入页高" required>
            </div>
          </div>

          <!-- 背景颜色 -->
          <div class="form-group">
            <label for="c3" class="col-sm-3 control-label">请选择背景颜色</label>&nbsp;&nbsp;&nbsp;
            <input type="radio" name="backColor" value="black">黑色 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="backColor" value="white" checked>白色
          </div>

          <!-- 是否设置文字转为形状 -->
          <div class="form-group">
            <label for="c4" class="col-sm-3 control-label">是否设置文字转为形状</label>&nbsp;&nbsp;&nbsp;
            <input type="radio" name="textAsShapes" value="1">是 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="textAsShapes" value="0" checked>否
          </div>

          <!-- 是否自动缩放 -->
          <div class="form-group">
            <label for="c5" class="col-sm-3 control-label">是否自动缩放</label>&nbsp;&nbsp;&nbsp;
            <input type="radio" name="automaticLayoutsScaling" value="1" id="special" onchange="a()" checked="checked">是 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="automaticLayoutsScaling" value="0" onchange="a()" >否
          </div>

          <!-- 设置绘图类型 -->
          <div class="form-group">
            <label for="c6" class="col-sm-3 control-label">设置绘图类型</label>&nbsp;&nbsp;&nbsp;
            <input type="radio" name="drawType" value="0" checked onmousemove="udc1()" onmouseleave="udc2()">UseDrawColor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="drawType" value="1" onmousemove="uoc1()" onmouseleave="uoc2()">UseObjectColor
            <div id="useDrawColor" style="display: none">
              <span class="text-angel">
                画笔颜色为黑色,建议使用
                <span style="color: red">白色</span>
                底色
              </span>
            </div>
            <div id="useObjectColor" style="display: none">
              <span class="text-angel">
                画笔颜色为
                <span style="color: red">彩色</span>
              </span>
            </div>
          </div>

          <!-- 是否缩放 -->
          <div class="form-group ishide" id="noScaling">
            <label for="c7" class="col-sm-3 control-label">是否缩放</label>&nbsp;&nbsp;&nbsp;
            <input type="radio" name="noScaling" value="1" id="special1" checked onchange="b()">是 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="noScaling" value="0" onchange="b()">否
          </div>

          <!-- 缩放系数 -->
          <div class="form-group ishide ishide1" id="zoom">
            <label for="c8" class="col-sm-3 control-label">缩放系数</label>
            <div class="col-sm-4">
              <input type="text" id="c8" name="zoom" class="form-control" value="1.0f" onmousemove="zoom1()" onmouseleave="zoom2()">
              <span class="text-angel" id="zoomText" style="display:none;">
                值 1 对应于精确拟合，低于 1 的值允许保留边距，高于 1 的值允许放大绘图。
                <span style="color: red">在后边一定加f</span>
              </span>
            </div>
          </div>

          <!-- 设置笔画类型 -->
          <div class="form-group">
            <label for="c9" class="col-sm-3 control-label">设置笔画类型</label>&nbsp;&nbsp;&nbsp;
            <input type="radio" name="penType" value="1" checked onmousemove="square1()" onmouseleave="square2()">Square &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="penType" value="2" onmousemove="round1()" onmouseleave="round2()">Round
            <div id="square" style="display: none">
              <span class="text-angel">
                画笔绘制
                <span style="color: red">有棱角</span>
              </span>
            </div>
            <div id="round" style="display: none">
              <span class="text-angel">
                画笔绘制
                <span style="color: red">无棱角</span>
              </span>
            </div>
          </div>

          <!-- 绕X轴旋转角度 -->
          <div class="form-group">
            <label for="c10" class="col-sm-3 control-label">绕 X 轴旋转角度</label>
            <div class="col-sm-4">
              <input type="text" id="c10" class="form-control" name="XAngle" value="0" required>
              <span class="text-angel">请输入绕 X 轴旋转角度</span>
            </div>
          </div>

          <!-- 绕Y轴旋转角度 -->
          <div class="form-group">
            <label for="c11" class="col-sm-3 control-label">绕 Y 轴旋转角度</label>
            <div class="col-sm-4">
              <input type="text" id="c11" class="form-control" name="YAngle" value="0" required>
              <span class="text-angel">请输入绕 Y 轴旋转角度</span>
            </div>
          </div>

          <!-- 绕Z轴旋转角度 -->
          <div class="form-group">
            <label for="c12" class="col-sm-3 control-label">绕 Z 轴旋转角度</label>
            <div class="col-sm-4">
              <input type="text" id="c12" class="form-control" name="ZAngle" value="0" required>
              <span class="text-angel">请输入绕 Z 轴旋转角度</span>
            </div>
          </div>

          <!-- 上传文件 -->
          <div class="form-group">
            <label for="c13" class="col-sm-3 control-label">选择上传文件</label>
            <div class="col-sm-4">
              <input type="file" id="c13" name="file">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-success">开始转换</button>
            </div>
          </div>

        </form>
      </div>

      <!-- 表格 -->
      <c:if test="${fn:length(fileList) != 0}">
        <div class="fileList">
          <table class="table"  width="100%">

            <tr>
              <td class="tableTitle"></td>
              <td width="80%" class="tableTitle">转换成功列表</td>
              <td class="tableTitle"></td>
            </tr>
            <c:forEach var="fl" items="${fileList}" varStatus="status">
              <tr>
                <td>${status.index + 1}</td>
                <td>${fl}</td>
                <td>
                  <a href="/download/${fl}">
                    <button class="btn btn-info">下载</button>
                  </a>
                </td>
              </tr>
            </c:forEach>
          </table>
        </div>
      </div>
      </c:if>



  </div>
</body>
<%
  String msg = (String) session.getAttribute("msg");
  if (!msg.equals("0"))
    out.print("<script>alert('" + msg + "');</script>");
%>
<script  type="text/javascript">
  $(document).ready(function () {
    if ($("#special").is(':checked')) {
      $('.ishide').css('display', 'none')
      $('.fileList').css('margin-top', '-605px')
    } else {
      $(".ishide").show();
      $('.fileList').css('margin-top', '-700px')
    }


  });
  function a(){
    if ($("#special").is(':checked')) {
      $('.ishide').css('display', 'none')
      $('.fileList').css('margin-top', '-555px')
    } else {
      $(".ishide").show();
      $('.fileList').css('margin-top', '-650px')
    }
  }
  function b(){
    if ($("#special1").is(':checked')) {
      $(".ishide1").show();
    } else {
      $('.ishide1').css('display', 'none')
    }
  }
  function udc1() {
    $('#useDrawColor').css('display', 'block')
  }
  function udc2() {
    $('#useDrawColor').css('display', 'none')
  }
  function uoc1() {
    $('#useObjectColor').css('display', 'block')
  }
  function uoc2() {
    $('#useObjectColor').css('display', 'none')
  }
  function square1() {
    $('#square').css('display', 'block')
  }
  function square2() {
    $('#square').css('display', 'none')
  }
  function round1() {
    $('#round').css('display', 'block')
  }
  function round2() {
    $('#round').css('display', 'none')
  }
  function zoom1() {
    $('#zoomText').css('display', 'block')
  }
  function zoom2() {
    $('#zoomText').css('display', 'none')
  }
</script>
</html>