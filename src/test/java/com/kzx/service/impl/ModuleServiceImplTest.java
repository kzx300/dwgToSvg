package com.kzx.service.impl;

import com.kzx.pojo.Module;
import com.kzx.service.ModuleService;
import junit.framework.TestCase;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

/**
 * @author kongz
 * @version 1.0 2021/8/13 11:28
 * @description TODO
 */
public class ModuleServiceImplTest extends TestCase {

    private static ModuleService getService() {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        return context.getBean("moduleServiceImpl", ModuleServiceImpl.class);
    }

    public void testQueryFirstStage() {
        System.out.println(getService().queryFirstStage());
    }

    public void testQuerySecondStage() {
        System.out.println(getService().querySecondStage("家具"));
    }

    public void testQueryThirdStage() {
        System.out.println(getService().queryThirdStage("家具", "桌子"));
    }

    public void testAddModule() {
        /*添加一级标题
        Module module = new Module();
        module.setCreateTime(new Date());
        module.setFirstStage("机械");
        module.setStage(1);
        System.out.println(getService().addModule(module));
        System.out.println(getService().queryFirstStage());*/

        /*添加二级标题
        Module module = new Module();
        module.setCreateTime(new Date());
        module.setFirstStage("机械");
        module.setSecondStage("齿轮");
        module.setStage(2);
        System.out.println(getService().addModule(module));
        System.out.println(getService().queryFirstStage());*/

        //添加三级标题
        Module module = new Module();
        module.setCreateTime(new Date());
        module.setFirstStage("机械");
        module.setSecondStage("齿轮");
        module.setName("普通齿轮");
        module.setStage(3);
        System.out.println(getService().addModule(module));
        System.out.println(getService().queryFirstStage());
    }

    public void testDeleteModule() {
        System.out.println(getService().deleteModule("020101"));
    }
}