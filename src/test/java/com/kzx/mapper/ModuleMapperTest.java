package com.kzx.mapper;

import com.kzx.pojo.Module;
import junit.framework.TestCase;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author kongz
 * @version 1.0 2021/8/12 21:07
 * @description TODO
 */
public class ModuleMapperTest extends TestCase {

    private static ModuleMapper getMapper() {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        return context.getBean("moduleMapper", ModuleMapper.class);
    }

    public void testGet() {
        /*ModuleMapper mapper = getMapper();
        System.out.println(mapper.get("1"));*/
        String id = "123456";
        System.out.println(Integer.parseInt(id.substring(0, 2)));
    }

    public void testQuery() {
        Map<String, Object> map = new HashMap<>();
        map.put("stage", 2);
        map.put("firstStage", "家具");
        map.put("secondStage", "桌子");
        for (Module module : getMapper().query(map)) {
            System.out.println(module);
        }
    }
}