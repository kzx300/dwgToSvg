package com.kzx.util;

import com.aspose.cad.Color;
import com.aspose.cad.Image;
import com.aspose.cad.fileformats.ObserverPoint;
import com.aspose.cad.imageoptions.CadRasterizationOptions;
import com.aspose.cad.imageoptions.PenOptions;
import com.aspose.cad.imageoptions.SvgOptions;
import com.kzx.pojo.SwitchOptions;

/**
 * @author kongz
 * @version 1.0 2021/8/3 19:17
 * @description TODO
 */
public class CADUtils {

    /**
     * @description dwg转svg工具方法
     * @param sourcePath 目标文件
     * @param prePath 文件存储地址
     * @return 转换成功文件名
     */
    public static String dwgToSvg(String sourcePath, String prePath) {
        return dwgToSvg(sourcePath, new SwitchOptions(), prePath);
    }

    /**
     * @description dwg转svg工具方法
     * @param sourcePath 目标文件
     * @param switchOptions 转换设置
     * @param prePath 文件存储地址
     * @return 转换成功文件名
     */
    public static String dwgToSvg(String sourcePath, SwitchOptions switchOptions, String prePath){
        String targetFileName = sourcePath.substring(0, sourcePath.lastIndexOf("."))+".svg";
        sourcePath = prePath + "upload\\" + sourcePath;
        String targetPath = prePath +  "svg\\" + targetFileName;
        SvgOptions options = new SvgOptions();
        setSvgOption(options, switchOptions);
        try(Image image = Image.load(sourcePath)) {
            CadRasterizationOptions cadRasterizationOptions = new CadRasterizationOptions();
            setCadRasterizationOptions(cadRasterizationOptions, switchOptions);
            options.setVectorRasterizationOptions(cadRasterizationOptions);
            image.save(targetPath, options);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        return targetFileName;
    }

    private static void setSvgOption(SvgOptions svgOptions, SwitchOptions switchOptions) {
        svgOptions.setTextAsShapes(switchOptions.getTextAsShapes().equals("1"));
    }

    private static void setCadRasterizationOptions(CadRasterizationOptions cadRasterizationOptions, SwitchOptions switchOptions) {
        cadRasterizationOptions.setPageWidth(switchOptions.getPageWidth());
        cadRasterizationOptions.setPageHeight(switchOptions.getPageHeight());
        cadRasterizationOptions.setBackgroundColor(Color.fromName(switchOptions.getBackColor()));
        cadRasterizationOptions.setAutomaticLayoutsScaling(switchOptions.getAutomaticLayoutsScaling().equals("1"));
        cadRasterizationOptions.setDrawType(switchOptions.getDrawType());
        if (switchOptions.getAutomaticLayoutsScaling().equals("0")) {
            cadRasterizationOptions.setNoScaling(switchOptions.getNoScaling().equals("1"));
            if (switchOptions.getNoScaling().equals("1"))
                cadRasterizationOptions.setZoom(switchOptions.getZoom());
        }
        PenOptions penOptions = new PenOptions();
        penOptions.setStartCap(switchOptions.getPenType());
        penOptions.setEndCap(switchOptions.getPenType());
        cadRasterizationOptions.setPenOptions(penOptions);
        cadRasterizationOptions.setObserverPoint(new ObserverPoint(
                switchOptions.getXAngle(), switchOptions.getYAngle(), switchOptions.getZAngle()
        ));
    }

}
