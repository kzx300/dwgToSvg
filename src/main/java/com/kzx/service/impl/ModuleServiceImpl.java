package com.kzx.service.impl;

import com.kzx.mapper.ModuleMapper;
import com.kzx.pojo.Module;
import com.kzx.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kongz
 * @version 1.0 2021/8/13 11:05
 * @description TODO
 */
@Service
public class ModuleServiceImpl implements ModuleService {

    @Autowired
    private ModuleMapper moduleMapper;

    @Override
    public List<Module> queryFirstStage() {
        Map<String, Object> map = new HashMap<>();
        map.put("stage", 1);
        return moduleMapper.query(map);
    }

    @Override
    public List<Module> querySecondStage(String firstStage) {
        Map<String, Object> map = new HashMap<>();
        map.put("stage", 2);
        map.put("firstStage", firstStage);
        return moduleMapper.query(map);
    }

    @Override
    public List<Module> queryThirdStage(String firstStage, String secondStage) {
        Map<String, Object> map = new HashMap<>();
        map.put("stage", 3);
        map.put("firstStage", firstStage);
        map.put("secondStage", secondStage);
        return moduleMapper.query(map);
    }

    @Override
    public boolean addModule(Module module) {
        Map<String, Object> map = new HashMap<>();
        switch (module.getStage()) {
            case 1: {
                List<Module> modules = queryFirstStage();
                int firstID = Integer.parseInt(modules.get(modules.size() - 1).getId().substring(0, 2));
                firstID += 1;
                if (firstID < 10)
                    module.setId("0" + firstID + "0000");
                else
                    module.setId(firstID + "0000");
                break;
            }
            case 2: {
                List<Module> modules = querySecondStage(module.getFirstStage());
                if (modules.size() == 0) {
                    map.put("firstStage", module.getFirstStage());
                    modules = moduleMapper.query(map);
                }
                int firstID = Integer.parseInt(modules.get(modules.size() - 1).getId().substring(0, 4));
                firstID += 1;
                if (firstID < 1000)
                    module.setId("0" + firstID + "00");
                else
                    module.setId(firstID + "00");
                break;
            }
            case 3: {
                List<Module> modules = queryThirdStage(module.getFirstStage(), module.getSecondStage());
                if (modules.size() == 0) {
                    map.put("firstStage", module.getFirstStage());
                    map.put("secondStage", module.getSecondStage());
                    modules = moduleMapper.query(map);
                }
                int firstID = Integer.parseInt(modules.get(modules.size() - 1).getId());
                firstID += 1;
                if (firstID < 100000)
                    module.setId("0" + firstID);
                else
                    module.setId(String.valueOf(firstID));
                break;
            }
        }
        return moduleMapper.add(module) == 1;
    }

    @Override
    public boolean deleteModule(String id) {
        return moduleMapper.delete(id) == 1;
    }
}
