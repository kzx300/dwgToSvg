package com.kzx.service;

import com.kzx.pojo.Module;

import java.util.List;

/**
 * @author kongz
 * @version 1.0 2021/8/13 10:49
 * @description TODO
 */
public interface ModuleService {

    //查询一级标题
    List<Module> queryFirstStage();

    //查询二级标题
    List<Module> querySecondStage(String firstStage);

    //查询三级标题
    List<Module> queryThirdStage(String firstStage, String secondStage);

    //新建分组
    boolean addModule(Module module);

    //删除分组
    boolean deleteModule(String id);

}
