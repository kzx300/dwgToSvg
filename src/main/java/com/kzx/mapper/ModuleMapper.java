package com.kzx.mapper;

import com.kzx.pojo.Module;

import java.util.List;
import java.util.Map;

/**
 * @author kongz
 * @version 1.0 2021/8/12 20:43
 * @description TODO
 */
public interface ModuleMapper {

    //根据id查询
    Module get(String id);

    //根据条件查询
    List<Module> query(Map<String, Object> map);

    //插入
    int add(Module module);

    //更新
    int update(Module module);

    //删除
    int delete(String id);

}
