package com.kzx.pojo;

import java.util.Date;

/**
 * @author kongz
 * @version 1.0 2021/8/12 19:08
 * @description 组件类别实体类
 */
public class Module {

    private String id;
    private String firstStage;
    private String secondStage;
    private String name;
    private Date createTime;
    private int stage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstStage() {
        return firstStage;
    }

    public void setFirstStage(String firstStage) {
        this.firstStage = firstStage;
    }

    public String getSecondStage() {
        return secondStage;
    }

    public void setSecondStage(String secondStage) {
        this.secondStage = secondStage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public String getPath() {
        StringBuilder path = new StringBuilder(firstStage);
        if (secondStage != null) {
            path.append("\\").append(secondStage);
        }
        path.append("\\").append(name).append(".svg");
        return path.toString();
    }

    @Override
    public String toString() {
        return "Module{" +
                "id='" + id + '\'' +
                ", firstStage='" + firstStage + '\'' +
                ", secondStage='" + secondStage + '\'' +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                ", stage=" + stage +
                '}';
    }
}
