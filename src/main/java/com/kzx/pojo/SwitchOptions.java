package com.kzx.pojo;

/**
 * @author kongz
 * @version 1.0 2021/8/4 15:48
 * @description TODO
 */
public class SwitchOptions {

    //页宽
    private float pageWidth;

    //页高
    private float pageHeight;

    //背景颜色
    private String backColor;

    //是否设置文字转换为形状 0or1
    //对应false or true
    private String textAsShapes;

    //是否自动缩放 0or1
    //对应false or true
    private String automaticLayoutsScaling;

    //设置绘图类型 0or1
    //对应CadDrawTypeMode.UseDrawColor/CadDrawTypeMode.UseObjectColor
    private int drawType;

    //是否缩放 0or1
    //对应false or true
    private String noScaling;

    //放缩系数
    //值 1 对应于精确拟合，低于 1 的值允许保留边距，高于 1 的值允许放大绘图。
    private float zoom;

    //设置画笔类型 1or2
    //Square or Round
    private int penType;

    //绕x轴旋转角度
    private float XAngle;

    //绕y轴旋转角度
    private float YAngle;

    //绕z轴旋转角度
    private float ZAngle;

    public SwitchOptions() {
        this.pageWidth = 4800;
        this.pageHeight = 3200;
        this.backColor = "white";
        this.textAsShapes = "0";
        this.automaticLayoutsScaling = "1";
        this.drawType = 0;
        this.noScaling = "1";
        this.zoom = 1.0f;
        this.penType = 2;
        this.XAngle = 0;
        this.YAngle = 0;
        this.ZAngle = 0;
    }

    public float getPageWidth() {
        return pageWidth;
    }

    public void setPageWidth(float pageWidth) {
        this.pageWidth = pageWidth;
    }

    public float getPageHeight() {
        return pageHeight;
    }

    public void setPageHeight(float pageHeight) {
        this.pageHeight = pageHeight;
    }

    public String getTextAsShapes() {
        return textAsShapes;
    }

    public String getBackColor() {
        return backColor;
    }

    public void setBackColor(String backColor) {
        this.backColor = backColor;
    }

    public void setTextAsShapes(String textAsShapes) {
        this.textAsShapes = textAsShapes;
    }

    public String getAutomaticLayoutsScaling() {
        return automaticLayoutsScaling;
    }

    public void setAutomaticLayoutsScaling(String automaticLayoutsScaling) {
        this.automaticLayoutsScaling = automaticLayoutsScaling;
    }

    public int getDrawType() {
        return drawType;
    }

    public void setDrawType(int drawType) {
        this.drawType = drawType;
    }

    public String getNoScaling() {
        return noScaling;
    }

    public void setNoScaling(String noScaling) {
        this.noScaling = noScaling;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    public int getPenType() {
        return penType;
    }

    public void setPenType(int penType) {
        this.penType = penType;
    }

    public float getXAngle() {
        return XAngle;
    }

    public void setXAngle(float XAngle) {
        this.XAngle = XAngle;
    }

    public float getYAngle() {
        return YAngle;
    }

    public void setYAngle(float YAngle) {
        this.YAngle = YAngle;
    }

    public float getZAngle() {
        return ZAngle;
    }

    public void setZAngle(float ZAngle) {
        this.ZAngle = ZAngle;
    }

    @Override
    public String toString() {
        return "SwitchOptions{" +
                "pageWidth=" + pageWidth +
                ", pageHeight=" + pageHeight +
                ", textAsShapes='" + textAsShapes + '\'' +
                ", automaticLayoutsScaling='" + automaticLayoutsScaling + '\'' +
                ", drawType='" + drawType + '\'' +
                ", noScaling='" + noScaling + '\'' +
                ", zoom=" + zoom +
                ", penType='" + penType + '\'' +
                ", XAngle=" + XAngle +
                ", YAngle=" + YAngle +
                ", ZAngle=" + ZAngle +
                '}';
    }
}
